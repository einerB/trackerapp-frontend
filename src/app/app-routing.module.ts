import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//Imports components
import { LoginComponent } from './login/login/login.component';
import { PasswordRecoveryComponent } from './login/password-recovery/password-recovery.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'password-recovery', component: PasswordRecoveryComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
